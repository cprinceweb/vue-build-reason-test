import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    menuItems: [
      { id: 1, title: "Home", url: "/" },
      { id: 2, title: "What we do", url: "about" },
      { id: 3, title: "The digital divide", url: "digital-divide" },
      { id: 4, title: "Get involved", url: "get-involved" },
      { id: 5, title: "Our network", url: "our-network" },
      { id: 6, title: "Insights", url: "insights" },
    ],
    blocks: [
      {
        id: 1,
        title: "Get online week 2021",
        content:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt libero vehicula, iaculis lorem quis, vehicula enim. Proin viverra in mi eu maximus. Sed aliquet venenatis pharetra.",
        link: 4,
        columns: 1,
      },
      {
        id: 2,
        title:
          "We are a social change charity, helping people to improve their lives through digital",
        content:
          "We tackle the most pressing issues of our time, working with partners in thousands of communities accross the UK and further afield",
        link: 2,
        columns: 2,
      },
    ],
    posts: [
      {
        id: 1,
        title: "Get online week",
        description: "Lorem ipsum dolor",
        url: "/",
      },
      {
        id: 2,
        title: "Learn my way",
        description: "Lorem ipsum dolor",
        url: "/",
      },
      {
        id: 3,
        title: "Make it click",
        description: "Lorem ipsum dolor",
        url: "/",
      },
      {
        id: 4,
        title: "Digital you",
        description: "Lorem ipsum dolor",
        url: "/",
      },
    ],
  },
  getters: {
    listMenuItems: (state) => {
      return state.menuItems;
    },
    getMenuItemById: (state) => (id) => {
      return state.menuItems.find((menuItems) => menuItems.id === id);
    },
    loopBlocks: (state) => {
      return state.blocks;
    },
    loopPosts: (state) => {
      return state.posts;
    },
  },
  mutations: {},
  actions: {},
  modules: {},
});
